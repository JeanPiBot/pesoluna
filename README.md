# Peso en la Luna

>  Obteniendo el peso en la luna

## Formula para Calcular el peso en la Luna

Esta calculadora calcula el peso en la luna del peso del objeto sobre la tierra. Siendo que toma el peso de un objeto en la tierra y lo convierte en peso de lunarn, la fórmula es Peso en la Luna = (Peso en la Tierra/9,81m/s2) * 1,622m/s2. Para calcular el peso en la luna, dividimos el peso en la tierra por la fuerza de gravedad de la tierra, que es 9,81m/s2. Esto calcula la masa del objeto. Una vez que tenemos la masa del objeto, podemos encontrar el peso multiplicándolo por la fuerza gravitacional a la que está sujeto. Siendo que la Luna tiene una fuerza gravitatoria de 1,622m/s2, multiplicamos la masa del objeto por esta cantidad para calcular el peso de un objeto sobre la luna.


### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
